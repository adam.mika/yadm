# My .dotfiles

## Bootstrap

```bash
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
eval "$(/opt/homebrew/bin/brew shellenv)"
/opt/homebrew/bin/brew install yadm
/opt/homebrew/bin/yadm clone git@gitlab.com:adam.mika/yadm.git
```


### SSH key

- https://medium.com/risan/upgrade-your-ssh-key-to-ed25519-c6e8d60d3c54


```bash
ssh-keygen -o -a 100 -t ed25519 -f ~/.ssh/id_ed25519 -C "adam@macos"

eval "$(ssh-agent -s)"
ssh-add -K ~/.ssh/id_ed25519
```
